function hsize(b){
    if(b >= 1048576) return (Math.round((b / 1048576) * 100) / 100) + " mb";
    else if(b >= 1024) return (Math.round((b / 1024) * 100) / 100) + " kb";
    else return b + " b";
}

function split_filename(f){
    s = f.split('.');
    e = s.pop();
    n = s.join('.');

    return {
        name: n,
        extension: e.toLowerCase()
    };
}

function init_uploader(vars){
    var counter = 0;
    var fname_prefix = vars.filename_prefix;
    var total_queued = 0;
    var total_uploaded = 0;
    var total_failed = 0;
    var total_bytes_to_upload = 0;
    
    var kaltura = {
        format: 1,
        partner_id: vars.kaltura_partner_id,
        subp_id: vars.kaltura_subpartner_id,
        uid: vars.ids.uid,
        screenName: vars.ids.uid_name,
        ks: vars.ks
    };
    var images = vars.images;
    var videos = vars.videos;
    var audios = vars.audios;
    var obj_extensions = jQuery.extend({}, images, videos, audios);
    var extensions = [];

    var swfupload_instance = null;

    for(var i in obj_extensions){
        extensions.push(obj_extensions[i]);
    }

    jQuery('.swfupload-control').swfupload({
        // Backend Settings
        upload_url: vars.kaltura_api_url + '/upload',
        post_params: kaltura,

        // File Upload Settings
        // file_size_limit: '1.5 GB',
        // file_size_limit: vars.max_file_size,
        file_size_limit: 0,
        file_types: extensions.join(';'),
        file_types_description: 'Supported files',
        file_upload_limit: '50',
        file_queue_limit: '0',
        
        // Button Settings
        button_image_url: button_image_url, // Relative to the SWF file
        button_placeholder_id: 'spanButtonPlaceholder',
        button_text: '<span class="btn"><b>Upload...</b></span>',
        button_text_style: '.btn { font-size: 12; font-family: Arial; color: #1462A4; text-align: center; }',
        button_cursor: SWFUpload.CURSOR.HAND,
        button_width: 160,
        button_height: 22,

        // Flash Settings
        flash_url: vars.base_url + "/" + vars.path + '/swfupload.swf'
    })
    .bind('swfuploadLoaded', function(event){
        swfupload_instance = jQuery.swfupload.getInstance('.swfupload-control');

        jQuery('#pbdoctor-upload-form').hide();
        form = jQuery('<form id="frm-upload"><ul id="filelist"></ul><input id="btn-upload" type="button" class="form-submit" style="display: none;" value="Start upload"></form>');
        jQuery('#btn-upload', form).click(function(){
            total_queued = swfupload_instance.getStats().files_queued;
            //alert('total files to upload ' + total_queued);
            //alert('size of files '+ total_bytes_to_upload + ' space remained '+ vars.space_remaining);
            if(total_queued > 0){
                if(total_bytes_to_upload > vars.space_remaining){
                    var msg = "You have selected to upload a total of " + hsize(total_bytes_to_upload) + ".\n";
                    msg += "However, you only have " + hsize(vars.space_remaining) + " of remaining space.\n";

                    if(vars.can_upgrade){
                        msg += "Press OK to go to upgrade page or CANCEL to remove excess files from the upload queue.";
                        if(confirm(msg)) window.location.href = vars.base_url + "/upgrade";
                    }
                    else{
                        msg += "Please remove excess files from the upload queue to be able to upload.";
                        alert(msg);
                    }
                }
                else{
                    setButtonText("Uploading, please wait...", swfupload_instance);
                    swfupload_instance.setButtonDisabled(true);
                    swfupload_instance.setButtonCursor(SWFUpload.CURSOR.ARROW);
                    jQuery("#filelist div.filestat").empty().html('<i>Queued</i>');
                    jQuery('.swfupload-control').swfupload('startUpload');
                }
            }
            else alert("Please select files to upload.");
        });
        jQuery('.swfupload-control').after(form);
    })
    .bind('fileQueued', function(event, file){
        split_file = split_filename(file.name);
        if(!obj_extensions[split_file.extension]) return false;
        var fid = file.id;
        //alert('file id '+ fid);
        var fsize = file.size;
        var li = jQuery('<li id="file-' + fid + '"></li>');
        li
        .append('<div class="filename"><input type="text" id="filetitle-' + fid + '" style="width: 99%;" value="' + split_file.name + '"/></div>')
        .append('<div class="filesize">' + hsize(file.size) + '</div>')
        .append('<div class="filestat"></div>');

        /*if(videos[split_file.extension]){
          li.append('<div class="filetype"><label for="filetype-ultrasound-vid-' + fid + '"><input type="checkbox" id="filetype-ultrasound-vid-' + fid + '" /> Ultrasound</label></div>');
        }
        else if(images[split_file.extension]){
          li.append('<div class="filetype"><label for="filetype-ultrasound-img-' + fid + '"><input type="checkbox" id="filetype-ultrasound-img-' + fid + '" /> Ultrasound</label><label for="filetype-background-img-' + fid + '"><input type="checkbox" id="filetype-background-img-' + fid + '" /> Background</label></div>');
        }*/

        li.append('<div style="clear: both;"></div>');

        jQuery('<a href="#">Remove</a>').click(function(){
            try{
                jQuery('.swfupload-control').swfupload('cancelUpload', fid);
                li.remove();
                total_bytes_to_upload -= fsize;
                if(total_bytes_to_upload < 0) total_bytes_to_upload = 0;
                var stats = swfupload_instance.getStats();
                if(stats.files_queued > 0) jQuery('#btn-upload').show();
                else jQuery('#btn-upload').hide();
            }
            catch(e){}
            return false;
        })
        .appendTo(jQuery('div.filestat', li));
        jQuery('#filelist').append(li);
        jQuery('.swfupload-control').swfupload('addFileParam', file.id, 'filename', fname_prefix + '_' + counter);
        counter++;

        swfupload_instance = jQuery.swfupload.getInstance('.swfupload-control');
        //setButtonText("Select more files...", swfupload_instance);

        total_bytes_to_upload += file.size;
    })
    .bind('fileQueueError', function(event, file, errorCode, message){
        alert(file.name + ' - ' + message);
    })
    .bind('fileDialogStart', function(event){
        var stats = swfupload_instance.getStats();
        if(stats.files_queued < 1) jQuery('#filelist').empty();
    })
    .bind('fileDialogComplete', function(event, numFilesSelected, numFilesQueued){
        if(jQuery('#filelist li').length > 0) jQuery('#btn-upload').show();
        else jQuery('#btn-upload').hide();
    })
    .bind('uploadStart', function(event, file){
        jQuery('#btn-upload').hide();
        jQuery('#filetitle-' + file.id + ', #filetype-' + file.id).attr('readonly', 'readonly');
        jQuery('#file-' + file.id + ' div.filestat').empty().html('<i>Uploading</i>');

        // if(obj_extensions[split_filename(file.name).extension]) $('#file-' + file.id + ' div.filestat').empty().html('<i>Uploading</i>');
        // else return false;
    })
    .bind('uploadProgress', function(event, file, bytesLoaded){
        jQuery('#file-' + file.id + ' div.filestat').html('<i>Uploading (' + Math.round((bytesLoaded * 100) / file.size) + '%)</i>');
    })
    .bind('uploadSuccess', function(event, file, serverData){
        try{
            var data = eval('(' + serverData + ')');
            var result = data.result.result_ok;
            var media_type = 1;
            var fname_parts = split_filename(file.name);
            if(images[fname_parts.extension]) media_type = 2;
            else if(audios[fname_parts.extension]) media_type = 5;

            var addentry_params = jQuery.extend({}, kaltura, {
                file_size: file.size,
                is_doctor_upload: vars.is_doctor_upload,
                kshow_id: -1,
                entry_name: jQuery('#filetitle-' + file.id).val(),
                entry_filename: result.filename,
                entry_realFilename: file.name,
                entry_mediaType: media_type,
                entry_type: 1,
                entry_source: 1,
                entry_partnerData: vars.kaltura_partner_data
            });

            if(videos[fname_parts.extension]){
              if(jQuery('#filetype-ultrasound-vid-' + file.id).is(':checked')) jQuery.extend(addentry_params, { entry_conversionQuality: 4352531, entry_conversionProfile: 573441, entry_partnerData: 'ultrasound_image' });
            }
            else if(images[fname_parts.extension]){
                if(jQuery('#filetype-ultrasound-img-' + file.id).is(':checked')) jQuery.extend(addentry_params, { is_ultrasound_image: "yes" });
                if(jQuery('#filetype-background-img-' + file.id).is(':checked')) jQuery.extend(addentry_params, { entry_tags: 'background' });
            }

	  var flprox = new flensed.flXHR({autoUpdatePlayer: true, 
					  instanceId: file.id,
					  xmlResponseText: false,
					  onError: function (errObj) {
					    jQuery('#file-' + errObj.srcElement.instanceId + ' div.filestat').html('<b style="color: #ff0000;">Failed</b>');
					    total_failed++;
					  },
					  onreadystatechange: function (XHRobj) {
					    if (XHRobj.readyState == 4) {
					      jQuery('#file-' + XHRobj.instanceId + ' div.filestat').html('<b>Completed</b>');
					      jQuery.extend(addentry_params, {kaltura_response: XHRobj.responseText });
					      jQuery.ajax({
						       url: vars.base_url + '/upload_addentry',
						       type: 'POST',
						       data: addentry_params,
						       dataType: 'json',
						       complete: function() {
							 total_uploaded++;
							 if((total_failed + total_uploaded) >= total_queued)
							   setButtonText("Upload complete.", swfupload_instance);
							 if(total_uploaded >= total_queued)
							   window.location.href = vars.base_url + '/gallery_redir';
						       }
						     });

					    }
					  }
					  });
	  jQuery('#file-' + file.id + ' div.filestat').html('<i>Processing</i>');
	  flprox.open('POST',vars.kaltura_api_url + "/addentry");
	  var postvalues="";
	  for (var i in addentry_params) {
	    if (i != 'is_doctor_upload' &&
		i != 'file_size' &&
		i != 'is_ultrasound_image') {
	      postvalues += i + "=" + escape(addentry_params[i]) + "&";
	    }
	  }
	  flprox.send(postvalues);
	  if  (0) {
            jQuery.ajax({
                url: vars.base_url + '/upload_addentry',
                type: 'POST',
                data: addentry_params,
                dataType: 'json',
                beforeSend: function(){
                    jQuery('#file-' + file.id + ' div.filestat').html('<i>Processing</i>');
                    return true;
                },
                success: function(d){
                    try{
                    total_uploaded++;
                    jQuery('#file-' + file.id + ' div.filestat').html('<b>Completed</b>');
                    }
                    catch(e){
                            alert('success'+ e);
                    }},
                error: function(){
                    try{
                    total_failed++;
                    jQuery('#file-' + file.id + ' div.filestat').html('<b style="color: #ff0000;">Failed</b>');
                    }
                    catch(e){
                            alert('error' + e);
                    }},
                complete: function(){
                    if((total_failed + total_uploaded) >= total_queued) setButtonText("Upload complete.", swfupload_instance);
                    if(total_uploaded >= total_queued) window.location.href = vars.base_url + '/pbgallery_redir';
                }
            }); 
	  }
        }
        catch(e){
            total_failed++;
            jQuery('#file-' + file.id + ' div.filestat').html('<b style="color: #ff0000;">Failed</b>');
        }
    })
    .bind('uploadComplete', function(event, file){
        // upload has completed, lets try the next one in the queue
        if(total_queued - (total_uploaded + total_failed) > 0) jQuery(this).swfupload('startUpload');
    })
    .bind('uploadError', function(event, file, errorCode, message){
        total_failed++;
        jQuery('#file-' + file.id + ' div.filestat').html('<b style="color: #ff0000;">Failed</b>');
    });
}

function setButtonText(text, swfupload_instance){
    swfupload_instance.setButtonText("<span class='btn'><b>" + text + "</b></span>");
}


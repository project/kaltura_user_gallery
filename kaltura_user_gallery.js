jQuery(document).ready(function(){
  jQuery("#gallery-mine, #gallery-shared").listen("click", "a", function(){
        if(jQuery(this).hasClass('nolisten')) return true;
        if(jQuery(this).parents("#gallery-mine").length > 0) var $div = jQuery("#gallery-mine");
        else var $div = jQuery("#gallery-shared");

        jQuery("<div class='throbber'>Loading...</div>").appendTo($div);
        $div.load(jQuery(this).attr("href"), "_=" + new Date().getTime());
        return false;
    });

    //jQuery("#content-inner")
    jQuery("input.cb").live('click', function(){
        cb_change.call(this);
    })
    jQuery("#checkall").live('click', function(){
        if(jQuery(this).is(":checked")) jQuery("#frm_pbgallery :checkbox").attr("checked", "checked");
        else jQuery("#frm_pbgallery :checkbox").removeAttr("checked");

        jQuery("#frm_pbgallery :checkbox").not("#checkall").each(function(i){
            cb_change.call(this);
        });
    });

var remix_ids = jQuery.cookie("remix_ids");
if(remix_ids) remix_ids = remix_ids.split("#");
else remix_ids = new Array();


function cb_change(){
    $this = jQuery(this);
    var v = $this.val();

    var i = jQuery.inArray(v, remix_ids);
    if($this.is(":checked")){
        if(i < 0) remix_ids.push(v);
    }
    else{
        if(i >= 0) remix_ids.splice(i, 1);
    }

    jQuery.cookie("remix_ids", remix_ids.join("#"));

    if(jQuery("#frm_pbgallery input.cb:checked").length >= jQuery("#frm_pbgallery input.cb").length) jQuery("#checkall").attr("checked", "checked");
    else jQuery("#checkall").removeAttr("checked");

    if(remix_ids.length > 0) jQuery("#btn_remix").removeAttr("disabled");
    else jQuery("#btn_remix").attr("disabled", "disabled");

    // window.console.log(remix_ids);
}

});